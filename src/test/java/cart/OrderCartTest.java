package cart;

import org.junit.Before;
import org.junit.Test;
import rules.PricingRules;
import item.Item;

import java.util.InputMismatchException;

import static org.junit.Assert.assertEquals;


public class OrderCartTest {
    private PricingRules rules;

    @Before
    public void setUp() {
        this.rules = new PricingRules("ItemPrices.txt");
    }

    @Test
    public void testTotal() {
        assertEquals(0, price(""));
        assertEquals(50, price("A"));
        assertEquals(80, price("AB"));
        assertEquals(115, price("CDBA"));

        assertEquals(100, price("AA"));
        assertEquals(130, price("AAA"));
        assertEquals(180, price("AAAA"));
        assertEquals(230, price("AAAAA"));
        assertEquals(260, price("AAAAAA"));

        assertEquals(160, price("AAAB"));
        assertEquals(175, price("AAABB"));
        assertEquals(190, price("AAABBD"));
        assertEquals(190, price("DABABA"));
    }

    @Test
    public void testIncremental() {
        OrderCart cart = new OrderCart(rules);
        assertEquals(0, cart.calculateCartTotal());
        cart.addToCart(new Item("A"));
        assertEquals(50, cart.calculateCartTotal());
        cart.addToCart(new Item("B"));
        assertEquals(80, cart.calculateCartTotal());
        cart.addToCart(new Item("A"));
        assertEquals(130, cart.calculateCartTotal());
        cart.addToCart(new Item("A"));
        assertEquals(160, cart.calculateCartTotal());
        cart.addToCart(new Item("B"));
        assertEquals(175, cart.calculateCartTotal());
        cart.removeFromCart(new Item("A"));
        assertEquals(145, cart.calculateCartTotal());
        cart.removeFromCart(new Item("B"));
        assertEquals(130, cart.calculateCartTotal());
        cart.removeFromCart(new Item("B"), 10);
        assertEquals(100, cart.calculateCartTotal());
    }

    @Test(expected = InputMismatchException.class)
    public void testBadItemName() {
        OrderCart cart = new OrderCart(rules);
        Item item = new Item("X");
        cart.addToCart(item);
    }

    @Test(expected = InputMismatchException.class)
    public void testBadItemQuantity() {
        OrderCart cart = new OrderCart(rules);
        Item item = new Item("B");
        cart.addToCart(item, -50);
    }

    /**HELPER FUNCTIONS**/
    private int price(String itemList) {
        OrderCart cart = new OrderCart(rules);
        for(int i = 0; i < itemList.length(); i++) {
            String itemName = itemList.substring(i, i + 1);
            Item item = new Item(itemName);
            cart.addToCart(item);
        }

        return cart.calculateCartTotal();
    }
}
