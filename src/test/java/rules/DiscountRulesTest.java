package rules;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class DiscountRulesTest {
    private DiscountRules discountRule;

    @Before
    public void initialSetUp () {
        this.discountRule = new DiscountRules(5, 100);
    }

    @Test
    public void testIsDiscountApplicable() {
        assertTrue(discountRule.isDiscountApplicable());
        assertTrue(discountRule.isDiscountApplicable(12));
        assertTrue(discountRule.isDiscountApplicable(103));
        assertFalse(discountRule.isDiscountApplicable(3));
        assertFalse(discountRule.isDiscountApplicable(-20));
        changeDiscountRule(0, 0);
        assertFalse(discountRule.isDiscountApplicable());
    }

    @Test
    public void testGetDiscountedPrice() {
        initialSetUp();
        assertEquals(100, discountRule.getDiscountedPrice());
        assertEquals(200, discountRule.getDiscountedPrice(10));
        assertEquals(300, discountRule.getDiscountedPrice(15));
        changeDiscountRule(200, 1000);
        assertEquals(1000, discountRule.getDiscountedPrice(300));
        assertEquals(6000, discountRule.getDiscountedPrice(1200));
    }

    /*HELPER FUNCTION*/
    private void changeDiscountRule(int discountCount, int discountPrice) {
        discountRule = new DiscountRules(discountCount, discountPrice);
    }
}
