package rules;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ItemRuleTest {
    ItemRule rule;

    @Before
    public void setUp() {
        rule = new ItemRule(10, new DiscountRules(50, 400));
    }

    @Test
    public void testGetTotalCost() {
        assertEquals(200, rule.getTotalCost(20));
        assertEquals(50, rule.getTotalCost(5));
        assertEquals(400, rule.getTotalCost(40));
        assertEquals(400, rule.getTotalCost(50));
        assertEquals(4000, rule.getTotalCost(500));
        assertEquals(550, rule.getTotalCost(65));
    }
}
