package rules;

import item.Item;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class PricingRulesTest {
    private PricingRules rules;

    @Before
    public void setUp() {
        this.rules = new PricingRules("ItemPrices.txt");
    }

    @Test
    public void testCalculatePriceForItem () {
        assertEquals(130, rules.calculatePriceForItem(new Item("A"), 3));
        assertEquals(200, rules.calculatePriceForItem(new Item("C"), 10));
        assertEquals(165, rules.calculatePriceForItem(new Item("B"), 7));
    }

    @Test
    public void testIsPricingRulePresent () {
        assertTrue(rules.isPricingRulePresent(new Item("D")));
        assertFalse(rules.isPricingRulePresent(new Item("Z")));
    }
}
