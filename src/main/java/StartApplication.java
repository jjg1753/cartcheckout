import cart.OrderCart;
import item.Item;
import rules.PricingRules;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Logger;

public class StartApplication {
    private final static Logger LOGGER = Logger.getLogger(StartApplication.class.getName());
    private final static String ITEM_PRICES_FILE_NAME = "ItemPrices.txt";
    private PricingRules rules;
    private OrderCart cart;

    public StartApplication() {
        rules = new PricingRules(ITEM_PRICES_FILE_NAME);
        cart = new OrderCart(rules);
    }

    private void showCatalog() {
        System.out.println("Available items :");
        System.out.println("Name   |   Price   |   Discount offer");
        rules.displayPricingRules();
        System.out.println("---------------------------------------");
    }

    public static void main(String[] args) {
        StartApplication app = new StartApplication();
        app.showCatalog();

        Scanner sc = new Scanner(System.in);

        boolean checkedOut = false;
        int input;
        String[] inputItemQuantity;
        while (!checkedOut) {
            System.out.println("What do you want to do?(Enter a number)");
            System.out.println("1. Add an item to cart");
            System.out.println("2. Remove an item from the cart");
            System.out.println("3. Show cart");
            System.out.println("4. View catalog");
            System.out.println("5. Checkout");
            System.out.println("-----------------------------------------");
            input = sc.nextInt();

            switch (input) {
                case 1:
                    System.out.println("Adding items to cart...");
                    try {
                        inputItemQuantity = app.validateItemInput(sc);
                        app.cart.addToCart(new Item(inputItemQuantity[0]), Integer.parseInt(inputItemQuantity[1]));
                    } catch (InputMismatchException ime) {
                        LOGGER.warning(ime.getMessage());
                    } catch (NumberFormatException nfe) {
                        LOGGER.warning("Enter a valid number for quantity");
                    }
                    break;

                case 2:
                    System.out.println("Removing items from the cart...");
                    try {
                        inputItemQuantity = app.validateItemInput(sc);
                        app.cart.removeFromCart(new Item(inputItemQuantity[0]), Integer.parseInt(inputItemQuantity[1]));
                    } catch (InputMismatchException ime) {
                        LOGGER.warning(ime.getMessage());
                    } catch (NumberFormatException nfe) {
                        LOGGER.warning("Enter a valid number for quantity");
                    }
                    break;
                case 3:
                    app.cart.displayCart();
                    break;

                case 4:
                    app.showCatalog();
                    break;

                case 5:
                    System.out.println("Thank you! Order Placed...");
                    System.out.println("Final Cart");
                    app.cart.displayCart();
                    checkedOut = true;
                    break;

                default:
                    System.out.println("Invalid action selected. Try Again!");
                    break;
            }
        }
    }

    private String[] validateItemInput (Scanner sc) {
        System.out.println("Enter the item name and quantity (Example format: \"A 5\").");
        sc.nextLine(); // Discarding the unconsumed \n by nextInt
        String readLine = sc.nextLine();
        String[] splitted = readLine.split(" ");
        if (splitted.length == 2) {
            return splitted;
        } else {
            throw new InputMismatchException("Invalid input format. Try again!");
        }
    }
}
