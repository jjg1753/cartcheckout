package customexception;

public class ParsingFailureException extends RuntimeException {

    public ParsingFailureException(String s) {super(s);}
}
