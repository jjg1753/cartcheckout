package item;

import java.util.Objects;

/**
 * Author: Jeet Gandhi
 * Date: 3/3/21
 */

public class Item {
    private String name;

    public Item(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object equal) {
        if (this == equal) return true;

        if (equal == null || this.getClass() != equal.getClass()) return false;

        Item equalItemObject = (Item) equal;

        return Objects.equals(name, equalItemObject.name);
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return String.format("Item %s", getName());
    }
}