package rules;

public class ItemRule implements Rule {
    private int price;
    private DiscountRules discount;

    ItemRule(int price, DiscountRules discount) {
        this.price = price;
        this.discount = discount;
    }

    public int getTotalCost(int count) {
        if (discount.isDiscountApplicable(count)) {
            int discountedPrice = discount.getDiscountedPrice(count);
            int costForNonDiscountedItems = count % discount.getDiscountCount() * price;
            return discountedPrice + costForNonDiscountedItems;
        } else {
            return count * price;
        }
    }

    @Override
    public String toString() {
        return String.format("%d    |   %s", price, discount);
    }
}
