package rules;

/**
 * Author: Jeet Gandhi
 * Date: 3/3/21
 */
class DiscountRules {
    private int discountCount;
    private int discountedPrice;

    int getDiscountedPrice() {
        return discountedPrice;
    }

    int getDiscountCount() {
        return discountCount;
    }

    DiscountRules(int count, int discountedPrice) {
        this.discountCount = count;
        this.discountedPrice = discountedPrice;
    }

    boolean isDiscountApplicable() {return isDiscountApplicable(Integer.MAX_VALUE);}

    boolean isDiscountApplicable (int count) {
        return (discountCount > 0 && count >= discountCount);
    }

    int getDiscountedPrice(int count) {
        return (count / discountCount) * discountedPrice;
    }

    public String toString() {
        return isDiscountApplicable() ?
                String.format("%d for %d", getDiscountCount(), getDiscountedPrice()) : "Not Applicable";
    }
}