package rules;

import customexception.ParsingFailureException;
import item.Item;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class PricingRules {
    private final static Logger LOGGER = Logger.getLogger(PricingRules.class.getName());
    private Map<Item, Rule> itemRuleMap;
    private String rulesFileDestination;

    public PricingRules(String rulesFile) {
        this.rulesFileDestination = rulesFile;
        this.itemRuleMap = new HashMap<>();
        parsePricingRules();
    }

    private void parsePricingRules() {
        BufferedReader reader = null;
        String pricingRule = null;
        try {
            reader = new BufferedReader(new FileReader(rulesFileDestination));
            pricingRule = reader.readLine();
            while (pricingRule != null) {
                parseRuleLine(pricingRule);
                pricingRule = reader.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (NumberFormatException nfe) {
          throw new ParsingFailureException("Invalid format passed for item price in line : " + pricingRule);
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException ioe) {
                LOGGER.warning("Error while closing BufferedReader");
                ioe.printStackTrace();
            }
        }
    }

    private void parseRuleLine(String pricingRule) {
        String[] ruleComponents = pricingRule.split(",");
        String name = ruleComponents[0];

        int price = Integer.parseInt(ruleComponents[1]);
        int discountCount = 0;
        int discountPrice = 0;

        // This if condition is to check if a discount policy is present for a given item or not
        if (ruleComponents.length > 2) {
            String[] discountComponents = ruleComponents[2].split(" for ");
            discountCount = Integer.parseInt(discountComponents[0]);
            discountPrice = Integer.parseInt(discountComponents[1]);
        }

        itemRuleMap.put(new Item(name),
                new ItemRule(price, new DiscountRules(discountCount, discountPrice)));
    }

    // this function will be called from the cart to calculate total cost of each item
    // by applying each item rules respectively
    public int calculatePriceForItem (Item item, int itemCount) {
        Rule itemRule = itemRuleMap.get(item);

        if (itemRule == null) {
            throw new IllegalArgumentException("Unknown item found : " + item.getName());
        }
        return itemRule.getTotalCost(itemCount);
    }

    public boolean isPricingRulePresent(Item item) {
        return itemRuleMap.containsKey(item);
    }

    public void displayPricingRules() {
        for (Item item: itemRuleMap.keySet()) {
            ItemRule rule = (ItemRule) itemRuleMap.get(item);
            System.out.println(String.format("%s   |   %s", item, rule));
        }
    }
}
