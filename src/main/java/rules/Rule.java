package rules;

public interface Rule {
    int getTotalCost(int count);
}
