package cart;

import item.Item;
import rules.PricingRules;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.logging.Logger;

public class OrderCart {
    private final static Logger LOGGER = Logger.getLogger(OrderCart.class.getName());
    private Map<Item, Integer> cart;
    private PricingRules rules;

    public OrderCart(PricingRules rules) {
        this.cart = new HashMap<>();
        this.rules = rules;
    }

    public void addToCart(Item item) {
        addToCart(item, 1);
    }

    public void addToCart(Item item, int quantity) {
        if (rules.isPricingRulePresent(item)) {
            if (quantity > 0) {
                if (!cart.containsKey(item)) {
                    cart.put(item, quantity);
                } else {
                    cart.put(item, cart.get(item) + quantity);
                }
            } else {
                throw new InputMismatchException("Invalid quantity : " + quantity);
            }
        } else {
            throw new InputMismatchException(String.format("Item %s not present in catalog.", item));
        }
    }

    public void removeFromCart(Item item) {
        removeFromCart(item, 1);
    }

    public void removeFromCart(Item item, int quantity) {
        if (rules.isPricingRulePresent(item)) {
            if (quantity > 0) {
                if (cart.containsKey(item)) {
                    int existingQuantity = cart.get(item);
                    int quantityLeft = existingQuantity - quantity;
                    if (quantityLeft > 0) {
                        cart.put(item, quantityLeft);
                    } else {
                        cart.remove(item);
                    }
                } else {
                    LOGGER.warning("The item to be removed is not present in the cart. Try adding it first!");
                }
            } else {
                throw new InputMismatchException("Invalid quantity : " + quantity);
            }
        } else {
            throw new InputMismatchException(String.format("Item %s not present in the catalog.", item));
        }
    }

    public void displayCart() {
        System.out.println("-----------Cart Contents----------");
        System.out.println("Item    |   Quantity    |   Total");
        for (Item item: cart.keySet()) {
            System.out.println(String.format("%s    |   %d  |   %d",
                    item, cart.get(item), rules.calculatePriceForItem(item, cart.get(item))));
        }

        System.out.println();
        System.out.println("CART TOTAL = " + calculateCartTotal());
        System.out.println("---------------------------------");
        System.out.println();
    }

    int calculateCartTotal() {
        int total = 0;

        for(Item item : cart.keySet()) {
            int count = cart.get(item);
            total += rules.calculatePriceForItem(item, count);
        }
        return total;
    }
}
